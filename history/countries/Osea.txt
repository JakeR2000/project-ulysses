Capital = 94

oob = "DEB_1936"

set_research_slots = 3

set_stability = 0.5
set_war_support = 0.5


# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_support = 1		
	tech_engineers = 1
	gw_artillery = 1
	gwtank = 1
	early_fighter = 1
	CAS1 = 1
	early_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_battleship = 1
	transport = 1
}

set_politics = {
	parties = {
		democratic = { 
			popularity = 0
		}

		fascism = {
			popularity = 100
		}
		
		communism = {
			popularity = 0
		}
	}
}


create_country_leader = {
	name = "Hirohito"
	desc = "POLITICS_Hirohito_DESC"
	picture = "gfx/leaders/Debug/Nagato.dds"
	expire = "2010.1.1"
	ideology = fascism_ideology
	traits = {
		warrior_code
		emperor_showa
		imperial_sanction
	}
	id = 700 #Hirohito
}

create_country_leader = {
	name = "Kyuichi Tokuda"
	desc = "POLITICS_Hirohito_DESC"
	picture = "gfx/leaders/Debug/Nagato.dds"
	expire = "2010.1.1"
	ideology = marxism
	traits = {
	}
}

create_country_leader = {
	name = "Tetsu Katayama"
	desc = "POLITICS_Hirohito_DESC"
	picture = "gfx/leaders/Debug/Nagato.dds"
	expire = "2010.1.1"
	ideology = liberalism
	traits = {
	}
}

