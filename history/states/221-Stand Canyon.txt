
state={
	id=221
	name="STATE_221"
	provinces={
		967 989 1015 1030 1101 1143 1156 1189 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = AUR
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}