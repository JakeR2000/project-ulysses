
state={
	id=96
	name="STATE_96"
	provinces={
		2121 2230 2256 2291 2372 2388 2415 2429 2503 2542 2564 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = AUR
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}