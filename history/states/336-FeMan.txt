state={
	id=336
	name="STATE_336"
	provinces={
		5872 5980 6158 6167 6178 6212 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = DEB
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}