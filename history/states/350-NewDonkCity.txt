state={
	id=350
	name="STATE_350"
	provinces={
		7010 7038 7180 7195 7230 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = WVK
		add_core_of = WVK
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}