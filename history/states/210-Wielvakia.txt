
state={
	id=210
	name="STATE_210"
	provinces={
		7486 7601 7720 7750 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = WVK
		add_core_of = WVK
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}