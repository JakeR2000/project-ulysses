state={
	id=158
	name="STATE_158"
	provinces={
		2854 3173 3176 3334 3546 3691 3701 3719 3737 3801 3802 3840 3876 3890 3910 3950 4015 4022 4028 4084 4090 4162 4204 4225 4226 4350 4357 4367 4481 4568 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = DEB
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}