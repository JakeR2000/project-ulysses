state={
	id=353
	name="STATE_353"
	provinces={
		7302 7380 7409 7550 7572 7574 7656 7661 7748 7858 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = NRL
		add_core_of = NRL
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}