state={
	id=118
	name="STATE_118"
	provinces={
		7014 7254 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = YUK
		add_core_of = YUK
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}