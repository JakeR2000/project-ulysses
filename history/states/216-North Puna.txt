
state={
	id=216
	name="STATE_216"
	provinces={
		1353 1509 1561 1762 1815 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = AUR
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}