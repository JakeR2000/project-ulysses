state={
	id=85
	name="STATE_85"
	provinces={
		5408 5456 5592 5726 5748 5790 5802 5857 5887 5892 5962 6009 6027 6057 6195 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = OSE
		add_core_of = OSE
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}