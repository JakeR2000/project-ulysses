state={
	id=77
	name="STATE_77"
	provinces={
		4692 4785 4961 4965 5000 5070 5138 5214 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = OSE
		add_core_of = OSE
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}