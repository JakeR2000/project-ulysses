
state={
	id=11
	name="STATE_11"
	provinces={
		8472 8527 8746 8850 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = GEB
		add_core_of = GEB
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}