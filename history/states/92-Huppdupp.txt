state={
	id=92
	name="STATE_92"
	provinces={
		2953 3005 3015 3091 3196 3237 3264 3287 3375 3384 3427 3523 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = DEB
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}