
state={
	id=169
	name="STATE_169"
	provinces={
		6663 6750 6841 6875 6876 6899 6993 7008 7044 7065 7161 7174 7217 7327 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = FCU
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}