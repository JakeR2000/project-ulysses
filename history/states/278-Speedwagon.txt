state={
	id=278
	name="STATE_278"
	provinces={
		9895 9945 9984 10138 10208 10344 10538 10668 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = EVK
		add_core_of = EVK
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}