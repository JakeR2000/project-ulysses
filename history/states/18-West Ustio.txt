
state={
	id=18
	name="STATE_18"
	provinces={
		7964 8014 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = UST
		add_core_of = UST
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}