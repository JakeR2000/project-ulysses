state={
	id=62
	name="STATE_62"
	provinces={
		6709 6802 6838 6919 6982 7085 7118 7208 7263 7309 7419 7436 7540 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = OSE
		add_core_of = OSE
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}