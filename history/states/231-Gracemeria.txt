
state={
	id=231
	name="STATE_231"
	provinces={
		10583 10703 10735 10874 
}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category = megalopolis

	history={
		owner = EMM
		add_core_of = EMM
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 1
		}
}