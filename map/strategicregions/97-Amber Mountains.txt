
strategic_region={
	id=97
	name="STRATEGICREGION_97"
	provinces={
		6244 6310 6339 6386 6388 6410 6440 6443 6475 6486 6495 6522 6543 6548 6561 6577 6580 6586 6629 6650 6662 6680 6693 6713 6726 6744 6757 6761 6801 6804 6809 6840 6843 6869 6885 6895 6901 6903 6930 6950 6961 6976 7013 7018 7032 7109 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -5.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 0.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.050
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 3.0 6.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.150
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 0.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 8.0 17.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 11.0 20.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.0 22.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.0 21.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 9.0 18.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 2.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.050
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 0.0 8.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.210
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -3.0 5.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
