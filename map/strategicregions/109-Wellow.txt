strategic_region={
	id=109
	name="STRATEGICREGION_109"
	provinces={
		10355 10381 10487 10496 10607 10622 10660 10695 10775 10801 10827 10840 10892 10961 10962 11002 11027 11050 11065 11116 11133 11185 11197 11202 11245 11278 11283 11358 11371 11375 11406 11415 11416 11424 11428 11431 11432 11453 11517 11529 11571 11573 11578 11617 11624 11626 11629 11633 11678 11681 11692 11735 11750 11761 11831 11841 11850 11851 11862 11875 11878 11883 11986 11998 12002 12033 12041 12047 12063 12064 12072 12124 12127 12135 12218 12221 12226 12234 12250 12265 12270 12273 12308 12309 12370 12389 12447 12454 12465 12500 12503 12508 12558 12588 12590 12592 12609 12618 12673 12680 12694 12706 12729 12739 12760 12763 12778 12803 12858 12863 12875 12911 12937 12940 12943 12969 13001 13006 13038 13054 13072 13074 13130 13134 13155 13163 13166 13181 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -5.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.20
			blizzard=0.00
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 0.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.20
			blizzard=0.05
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 3.0 6.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.150
			blizzard=0.0
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 0.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 8.0 17.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 11.0 20.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.0 22.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.0 21.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 9.0 18.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 2.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.050
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 0.0 8.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.210
			blizzard=0.000
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -3.0 5.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.20
			blizzard=0.00
			mud=0.3
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}