
strategic_region={
	id=144
	name="STRATEGICREGION_144"
	provinces={
		5589 5732 5752 5777 5861 5918 5926 6049 6070 6105 6129 6203 6291 6321 6379 6412 6434 6468 6474 6482 6497 6500 6529 6566 6576 6585 6605 6616 6631 6637 6642 6673 6682 6705 6728 6729 6731 6732 6774 6775 6776 6784 6791 6803 6806 6808 6813 6818 6831 6844 6851 6857 6886 6897 6900 6905 6910 6928 6931 6934 6942 6954 6966 6979 6989 6995 7001 7014 7015 7016 7049 7062 7072 7076 7090 7114 7122 7143 7162 7179 7186 7191 7201 7237 7242 7254 7278 7301 7321 7333 7336 7449 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -5.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 0.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.050
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 3.0 6.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.150
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 0.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 8.0 17.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 11.0 20.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.0 22.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.0 21.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 9.0 18.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 2.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.050
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 0.0 8.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.210
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -3.0 5.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
