
strategic_region={
	id=146
	name="STRATEGICREGION_146"
	provinces={
		4171 4214 4379 4381 4441 4491 4518 4539 4546 4594 4603 4614 4642 4676 4685 4739 4760 4765 4795 4798 4809 4825 4872 4874 4883 4889 4891 4894 4896 4916 4935 4943 4976 4985 4990 4992 5003 5006 5014 5074 5075 5103 5137 5140 5168 5184 5193 5202 5238 5255 5262 5266 5297 5311 5328 5363 5417 5453 5505 5529 5554 5575 5604 5615 5622 5630 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -5.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 0.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.050
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 3.0 6.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.150
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 0.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 8.0 17.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 11.0 20.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.0 22.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.0 21.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 9.0 18.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 2.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.050
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 0.0 8.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.210
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -3.0 5.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
