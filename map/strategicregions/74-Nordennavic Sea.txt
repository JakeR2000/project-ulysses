
strategic_region={
	id=74
	name="STRATEGICREGION_74"
	provinces={
		10753 10803 10839 10886 10932 10943 10975 11001 11005 11048 11084 11090 11114 11142 11172 11176 11234 11237 11302 11312 11322 11365 11379 11442 11447 11466 11476 11481 11495 11499 11555 11560 11574 11596 11619 11662 11679 11723 11728 11780 11786 11830 11859 11876 11880 11881 11963 11990 12021 12067 12078 12084 12085 12090 12092 12095 12104 12105 12205 12207 12216 12274 12293 12328 12350 12365 12371 12377 12392 12403 12416 12450 12460 12475 12495 12560 12583 12603 12628 12637 12704 12757 12764 12775 12790 12794 12829 12843 12861 12880 12893 12898 12899 12915 12932 12946 13007 13008 13011 13026 13053 13061 13096 13133 13183 13199 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -5.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 0.0 7.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.050
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 3.0 6.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.150
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 0.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 8.0 17.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 11.0 20.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.0 22.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.0 21.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 9.0 18.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.000
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 2.0 12.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.050
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 0.0 8.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.210
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -3.0 5.0 }
			temperature_day_night={ 5.0 -5.0 }
			no_phenomenon=0.500
			rain_light=1.000
			rain_heavy=0.150
			snow=0.200
			blizzard=0.000
			mud=0.300
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
